(function () {
    'use strict';

    angular.module('Ranking').service('AgentRepository', ['$http', function ($http) {
        var self = this;		
		self.agents = [];
		
		self.FillAgents = function (completeResult) {
			var groupedAgents;			
			var tempAgentList = [];			
			
			//map the complete results to an array of agent names/Ids
			_.map(completeResult.Objects, function (entry) {
				tempAgentList.push({
					agentId: entry.MakelaarId,
					agentName: entry.MakelaarNaam
				});
			});
			
			//group the agents by Id
			groupedAgents = _.groupBy(tempAgentList, 'agentId');
						
			//reset the agents array, and fill her up with new data.
			self.agents = [];
			_.each(groupedAgents, function (agentGroup) {
				self.agents.push({
					agentId: _.head(agentGroup).agentId,
					agentName: _.head(agentGroup).agentName,
					amount: agentGroup.length
				});
			});
			
			//sort agents. Strictly speaking, this is a bit of view functionality handled by lodash.
			self.agents = _.sortBy(self.agents, ['amount']).reverse();
			
		};
		
		self.GetTopTen = function () {
			return _.take(self.agents, 10);			
		};
		
        return self;
    }]);
}());