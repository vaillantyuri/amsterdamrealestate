(function () {
    'use strict';
    angular.module('Ranking').component('topTen', {
        templateUrl: './ranking/ranking.view.html',
        controller:'RankingController',
        controllerAs:'ctrl'
    })
}());