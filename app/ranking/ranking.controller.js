(function () {
   'use strict';

   angular.module('Ranking').controller('RankingController', ['FundaService', 'AgentRepository', function (FundaService, AgentRepository) {       
	   var ctrl = this;
		ctrl.agents = [];
		
       ctrl.cityName = 'Amsterdam';
	   ctrl.withGarden = false;
	   
       ctrl.getEstates = function () {
		   var promise = FundaService.GetAllEstatesForCity(ctrl.cityName, ctrl.withGarden);	   
		   promise.then(ctrl.handleHouseResult);
       };
	
	   ctrl.handleHouseResult = function () {
		    var result = FundaService.result;
			AgentRepository.FillAgents(result);
			ctrl.agents = AgentRepository.GetTopTen();
	   };

	   ctrl.getEstates();
   }]);
}());