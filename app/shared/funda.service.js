(function () {
    'use strict';

    angular.module('Shared', []).service('FundaService', ['$http', '$q', 'FUNDA_CONFIG', function ($http, $q, FUNDA_CONFIG) {
        var self = this;

		self.pageSize = 1000; //set to default 1000, check later if we have all of the real estate on first call.
		self.page = 1; 
		self.type = 'koop';
		self.result = {};
		
		var endpoint = 'http://partnerapi.funda.nl/feeds/Aanbod.svc/JSON/' + FUNDA_CONFIG.apiKey;
		
		self.generateZo = function (cityName, withGarden) {
			return withGarden ? '/' + cityName + '/tuin/' : '/' + cityName + '/';			
		}
		
		
	    self.GetAllEstatesForCity = function (cityName, withGarden) {
			//create a custom promise for handling multiple request logic
			var deferred = $q.defer();
				$http({method:'GET', url:endpoint, params:{
						pagesize: self.pageSize,
						page:self.page,
						zo:self.generateZo(cityName, withGarden),
						type:self.type				
				}}).then(function (result) {
					// if the call was successful, and the object has all expected properties. 
					// but not all objects are present, get the missing objects.  
					if( angular.isDefined(result.data.Paging.AantalPaginas) && 
						result.data.Paging.AantalPaginas > 1 && 
						angular.isDefined(result.data.TotaalAantalObjecten)) {
							self.pageSize = result.data.TotaalAantalObjecten;															
							$http({method:'GET', url:endpoint, params:{
								pagesize: self.pageSize,
								page:self.page,
								zo:self.generateZo(cityName, withGarden),
								type:self.type					
							}}).then(function (result) {							
								self.result = result;							
								deferred.resolve(true);
							}, function (result) {
								deferred.reject(false);
							});
					} else if (result.data.Paging.aantalPaginas === 1) {
						self.result = result;
						deferred.resolve(true);
					}
				}, function (result) {
					deferred.reject(false);					
				});
				
				return deferred.promise;			
		};
		
        return self
    }]);
}());