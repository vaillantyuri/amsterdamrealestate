(function () {
    'use strict';
    
	
	//a mock service added to bypass CORS errors. 
    angular.module('Shared', []).service('FundaService', ['$http', '$q', 'FUNDA_CONFIG', function ($http, $q, FUNDA_CONFIG) {
        var self = this;

		self.pageSize = 1000; //set to default 1000, check later if we have all of the real estate on first call.
		self.page = 1; 
		self.type = 'koop';
		self.result = {};
		
		var endpoint = './shared/funda.response.json';
		
		self.generateZo = function (cityName, withGarden) {
			return withGarden ? '/' + cityName + '/tuin/' : '/' + cityName + '/';			
		}
		
	  self.GetAllEstatesForCity = function (cityName, withGarden) {
			var deferred = $q.defer();
				$http({method:'GET', url:endpoint, params:{
						pagesize: self.pageSize,
						page:self.page,
						zo:self.generateZo(cityName, withGarden),
						type:self.type				
				}}).then(function (result) {					
					if( angular.isDefined(result.data.Paging.AantalPaginas) && 
						result.data.Paging.AantalPaginas > 1 && 
						angular.isDefined(result.data.TotaalAantalObjecten)) {
							
							self.pageSize = result.data.TotaalAantalObjecten;	
							
							$http({method:'GET', url:endpoint, params:{
								pagesize: self.pageSize,
								page:self.page,
								zo:self.generateZo(cityName, withGarden),
								type:self.type					
							}}).then(function (result) {							
								self.result = result.data;							
								deferred.resolve(true);
							}, function (result) {
								deferred.reject(false);
							});
							
					} else if (result.data.Paging.AantalPaginas === 1) {
						self.result = result.data;
						deferred.resolve(true);
					}
				}, function (result) {
					deferred.reject(false);					
				});
				
				return deferred.promise;			
		};
		
        return self
    }]);
}());