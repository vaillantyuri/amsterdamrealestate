# `Funda app Exercise` — An exercise in Javascript UX

This is an example app demonstrating several aspects of a front-end client. It retrieves real estate data for the city of Amsterdam,
using it to render a list of 10 most well endowed agents, real-estate-wise.

!!!
Opening up index.html on a server is enough to make it work. Opening it on the local filesystem will create CORS errors,
due to your browsers strict Cross-Origin settings. Open the source folder in an IDE and/or run index.html from a server.

-->> 'npm run index.html'

## Dependencies:

This project was made in AngularJS, using Lodash for array handling
Javascript:
AngularJS, Lodash


###Conventions
- Single quotes in .js (out of habit)
- No service calling another service.
- Restricted use of $scope in controller.
- Every file encapsulated in module pattern and in strict mode.
- Capitalized public functions, pascalCased privates.